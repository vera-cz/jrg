package cz.vera.jrg.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class JRGUtilsTest {

	@Test
	public void test_getExtension(){
		String fn = null;
		assertTrue(JRGUtils.getExtension(fn).equals(""));

		fn = "pdf";
		assertTrue(JRGUtils.getExtension(fn).equals(""));
		
		fn = "file.pdf";
		assertTrue(JRGUtils.getExtension(fn).equals("pdf"));
		
		fn = "doc.rtf.exe";
		assertTrue(JRGUtils.getExtension(fn).equals("exe"));
	}
	
	@Test
	public void test_replaceExtension(){
		String fn = null;
		assertNull(JRGUtils.replaceExtension(fn, "pdf"));
		
		fn = "";
		assertEquals(JRGUtils.replaceExtension(fn, "pdf"), "");
		
		fn = "xx.rtf";
		assertEquals(JRGUtils.replaceExtension(fn, "pdf"), "xx.pdf");
		
		fn = "xx";
		assertEquals(JRGUtils.replaceExtension(fn, "pdf"), "xx.pdf");

		fn = "xx.rtf";		
		assertEquals(JRGUtils.replaceExtension(fn, ""), "xx.rtf");
	}
	
	@Test
	public void testGetFileNameFromPath(){
		String fn = JRGUtils.getFileNameFromPath("test"+JRGUtils.SEPARATOR+"subdir"+JRGUtils.SEPARATOR+"file.jpg");
		assertEquals("file.jpg", fn);

		fn = JRGUtils.getFileNameFromPath("test"+JRGUtils.SEPARATOR+"file");
		assertEquals("file", fn);

		fn = JRGUtils.getFileNameFromPath("test"+JRGUtils.SEPARATOR+"subdir"+JRGUtils.SEPARATOR);
		assertNull(fn);
		
		fn = JRGUtils.getFileNameFromPath(null);
		assertNull(fn);
	}
}
