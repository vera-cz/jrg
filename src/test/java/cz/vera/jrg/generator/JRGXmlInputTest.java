package cz.vera.jrg.generator;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import cz.vera.jrg.utils.JRGUtils;

import static org.junit.Assert.*;

public class JRGXmlInputTest {	
	private static final String XML_FILE = "src"+JRGUtils.SEPARATOR+"test"+JRGUtils.SEPARATOR+"resources"+JRGUtils.SEPARATOR+"input.xml";
	private JRGXmlInput input;
	
	@Before
	public void setUp(){
		input = new JRGXmlInput();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_validate_expectedIllegalArgumentException1(){
		input.setInputXmlFile("neexistujiciInput.xml");
		input.setJasperFile("neexistujiciSablona");
		input.setOutputFile("output.rtf");
		input.validate();
	}

	@Test(expected=IllegalArgumentException.class)
	public void test_validate_expectedIllegalArgumentException2(){	
		input.validate();
	}
	
	@Test(expected=NullPointerException.class)
	public void test_getJasperReport_NullPointerException() throws JRException{
		JasperReport jr = input.getJasperReport();		
	}
	
	/*
	@Test(expected=NullPointerException.class)
	public void test_getDocument_expectedNullPointerException() throws JRException{
		input.getDocument();		
	}

	@Test
	public void test_getDocument() throws JRException, XPathExpressionException{
		input.setInputXmlFile(XML_FILE);
		Document doc = input.getDocument();
		assertNotNull(doc);
		
		XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile("//data/ico/text()");

	    Object result = expr.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodes = (NodeList) result;	    
	    assertEquals("00261220", nodes.item(0).getNodeValue());
	}*/

}
