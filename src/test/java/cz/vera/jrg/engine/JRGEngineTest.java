package cz.vera.jrg.engine;

import org.junit.Before;

import cz.vera.jrg.generator.JRGXmlInput;
import cz.vera.jrg.utils.JRGUtils;

public abstract class JRGEngineTest {

	protected static final String XML_FILE 	    = "src"+JRGUtils.SEPARATOR+"test"+JRGUtils.SEPARATOR+"resources"+JRGUtils.SEPARATOR+"input.xml";
	protected static final String JASPER_FILE	= "target"+JRGUtils.SEPARATOR+"test-classes"+JRGUtils.SEPARATOR+"report1";	
	
	protected JRGXmlInput input;
	
	@Before
	public void setUp(){
		input = new JRGXmlInput();
		input.setInputXmlFile(XML_FILE);
		input.setJasperFile(JASPER_FILE);
	}

}
