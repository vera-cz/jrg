package cz.vera.jrg.engine;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import cz.vera.jrg.engine.JRGEngineFactory.EngineType;
import cz.vera.jrg.generator.JRGGenerator;
import cz.vera.jrg.utils.JRGUtils;

public class JRGPdfEngineTest extends JRGEngineTest{

	private static final String OUTPUT_FILE = "target"+JRGUtils.SEPARATOR+"output.pdf";
	
	
	@Test
	public void test_generateFileFromXMLFile(){
		input.setOutputFile(OUTPUT_FILE);
		File o = new File(OUTPUT_FILE);
		if(o.exists()){
			o.delete();
		}
		File x = new File(XML_FILE);
		assertTrue(x.exists());
						
		JRGGenerator gen = new JRGGenerator(JRGEngineFactory.getEngine(EngineType.PDF));
		gen.generate(input);
		
		File f = new File(OUTPUT_FILE);
		assertTrue(f.exists());
		
	}

}
