package cz.vera.jrg.engine;

import org.junit.Test;
import cz.vera.jrg.engine.JRGEngineFactory.EngineType;
import static org.junit.Assert.*;

public class EngineTypeTest {
	
	@Test
	public void test_getInstance(){
		EngineType type = EngineType.getInstance("pdf");
		assertEquals(EngineType.PDF, type);
		
		type = EngineType.getInstance("docx");
		assertEquals(EngineType.DOCX, type);
		
		type = EngineType.getInstance("XLS");
		assertEquals(EngineType.XLS, type);
		
		type = EngineType.getInstance("rtf");
		assertEquals(EngineType.RTF, type);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_getInstance_expectedIllegalArgumentException(){
		EngineType.getInstance("nesmysl");
	}
}
