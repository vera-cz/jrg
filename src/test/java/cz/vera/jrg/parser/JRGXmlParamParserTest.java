package cz.vera.jrg.parser;

import java.io.File;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.util.JRXmlUtils;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import static org.junit.Assert.*;

import cz.vera.jrg.utils.JRGUtils;

public class JRGXmlParamParserTest {
	private static final Logger LOGGER = Logger.getLogger(JRGXmlParamParserTest.class);
	private static final String S = JRGUtils.SEPARATOR;
	//private static final String XML_FILE = "src"+JRGUtils.SEPARATOR+"test"+JRGUtils.SEPARATOR+"resources"+JRGUtils.SEPARATOR+"params.xml";
	private static final String XML_FILE  = "src"+S+"test"+S+"resources"+S+"input.xml";
	
	//private static final String XML_FILE2  = "src"+S+"test"+S+"resources"+S+"input_big.xml";
	//private static final String XML_FILE2  = "src"+S+"test"+S+"resources"+S+"input_biggest.xml";//
	//private static final String XML_FILE2  = "src"+S+"test"+S+"resources"+S+"input_biggest_k.xml";//
	private static final String XML_FILE2 = XML_FILE;
	
	private JRGParamParser parser;
	
	@Before
	public void setUp(){
		parser = JRGXmlParamParser.getInstance();
	}
		
	@Test
	public void test_parseFromDocument() throws JRException {
		long timePred = System.currentTimeMillis();
		Document doc = JRXmlUtils.parse(new File(XML_FILE2));
		long timePo   = System.currentTimeMillis();
		LOGGER.debug("parsovani dokumentu:"+(timePo-timePred));
		
		
		timePred = System.currentTimeMillis();
		Map<String, Object> params = parser.parseFromDocument(doc);
		timePo   = System.currentTimeMillis();
		LOGGER.debug("parsovani parametru:"+(timePo-timePred));
		
		assertEquals(19, params.size());
		
		assertEquals("M�STSK� ��AD �ESK� KAMENICE", params.get("nazev_ico"));
		assertEquals("01/29/2009", params.get("datum"));
		assertEquals(null, params.get("trid3"));
		
		assertEquals(Boolean.TRUE, params.get("zobrazeni_datumu"));
	}
	
	@Test
	public void test_parseFromFile() {
		
		Map<String, Object> params = parser.parseFromFile(XML_FILE2);
		
		assertEquals(19, params.size());
		
		assertEquals("M�STSK� ��AD �ESK� KAMENICE", params.get("nazev_ico"));
		assertEquals("01/29/2009", params.get("datum"));
		assertEquals(null, params.get("trid3"));
	}

}
