package cz.vera.jrg.generator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import cz.vera.jrg.engine.JRGEngine;

/**
 * Gener�tor. ��d� postup generov�n� dokumentu. Zas�l� zpr�vy stroji.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:44
 */
public class JRGGenerator {
	private static final Logger LOGGER = Logger.getLogger(JRGGenerator.class);
	private JRGEngine engine;

	/**
	 * Bezparametrick� konstruktor
	 */
	public JRGGenerator(){
	}

	/**
	 * Konstruktor
	 * @param engine
	 */
	public JRGGenerator(JRGEngine engine){
		this.engine = engine;
	}

	/**
	 * Generuje report pomoc� jasper �ablony do souboru.
	 * @param input
	 */
	public void generate(JRGInput input){
		try {
			//engine.putParameter(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, input.getDocument());
			//engine.setDataSource(input.getDataSource());

			putParametersToEngine(input.getParameters(), engine);
			LOGGER.debug("Plnim report daty.");
			engine.fillReport(input.getJasperReport());
			LOGGER.debug("Exportuji report do souboru.");			
			if(engine.getPageCount()>0){
				engine.exportReport(input.getOutputFile());
				if(input.isCompressionDemanded()){
					LOGGER.debug("Komprimuji vystupni soubor.");
					engine.compressReport(input.getOutputFile());
				}
			}else{
				LOGGER.info("Nebyl nalezen ��dn� z�znam ke zpracov�n�. Dokument nebyl vytvo�en.");
			}
		} catch (JRException e) {		
			LOGGER.error("Chyba p�i generov�n� dokumentu: ", e);
		}
	}
	
	/**
	 * Nastav� enginu parametry inputu
	 * @param params
	 * @param engine
	 */
	private void putParametersToEngine(Map<Object, Object> params, JRGEngine engine){
		for (Entry<Object, Object> param : params.entrySet()) {
			engine.putParameter(param.getKey(), param.getValue());
		}		
	}
	
	/**
	 * Nastavuje stroj pro export dokumentu.
	 * @param engine
	 */
	public void setEngine(JRGEngine engine) {
		this.engine = engine;
	}
}