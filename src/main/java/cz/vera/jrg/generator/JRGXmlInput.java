package cz.vera.jrg.generator;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.naming.OperationNotSupportedException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRXmlUtils;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import cz.vera.jrg.parser.JRGParamParser;
import cz.vera.jrg.parser.JRGXmlParamParser;
import cz.vera.jrg.utils.JRGUtils;

/**
 * T��da vstupn�ch parametr� a dat pro export z XML do souboru.
 * 
 * @author STJ
 * @version 1.2
 * @created 16-II-2010 12:00:45
 */
public class JRGXmlInput implements JRGInput{
	private Logger LOGGER = Logger.getLogger(JRGXmlInput.class);
	
	/** Cesta k vstupn�mu xml.*/
	private String inputXmlFile;
	/** Cesta k vstupn�mu jasper souboru. (Bu� zkompilovan� jasper, nebo jrxml �ablona)*/
	private String jasperFile;
	/** Cesta k v�stupn�mu souboru. */
	private String outputFile;
	/** Zda je po�adovan� tak� komprimace v�stupu do zipu */
	private boolean compressionDemanded;
	/** Vstupn� xml dokument */
	private Document document;
	/** Cesta ke xml s parametry.*/
	private String paramsXmlFile;
	/**	Vstupn� parametry */
	private Map<Object, Object> parameters;
	/** Form�t v�stupu */
	private String format;
	
	public JRGXmlInput(){
	}

	/**
	 * Validuje vstup. Zji��uje zda existuj� zadan� vstupn� soubory. Pokud ne vyhazuje v�jimku.
	 */
	@Override
	public void validate() throws IllegalArgumentException{
		if(document == null){
			if(inputXmlFile == null || inputXmlFile.equals("")){
				throw new IllegalArgumentException("Nen� zad�n vstupn� XML soubor.");
			}else{
				File ixf = new File(inputXmlFile);
				if(!ixf.exists()){
					throw new IllegalArgumentException("Nenalezen vstupn� XML soubor: " + inputXmlFile);
				}			
			}
		}
		if(jasperFile == null || jasperFile.equals("")){
			throw new IllegalArgumentException("Nen� zad�na JasperReports �ablona.");
		}else{
			File jf = new File(jasperFile);
			if(!jf.exists()){
				throw new IllegalArgumentException("Nenalezena vstupn� JasperReports �ablona:" + jasperFile);
			}			
		}

		if(outputFile == null || outputFile.equals("")){
			throw new IllegalArgumentException("Nen� zad�n v�stupn� soubor.");
		}
		
	}
	
	@Override
	public void putParameter(String name, Object value) {	
		if(parameters == null){
			parameters = new HashMap<Object, Object>();
		}
		parameters.put(name, value);
	}

	public void putParameters(Map<String, Object> params) {
		if (params!=null){
			for (Entry<String, Object> param : params.entrySet()) {
				putParameter(param.getKey(), param.getValue());
			}
		}
	}

	@Override
	public JRDataSource getDataSource() {
		try {
			JRXmlDataSource ds = new JRXmlDataSource(inputXmlFile, "".trim());
			return ds;
		} catch (JRException e) {
			// do nothing? 
		}
		return null;
	}
	
	/**
	 * Zkompiluje jasper �ablonu do souboru.
	 * 
	 * @param reportPathname
	 * @throws OperationNotSupportedException 
	 */
	protected void compileReportToFile(String reportPathname) throws OperationNotSupportedException{
		throw new OperationNotSupportedException("Program zat�m neumo��uje kompilaci report�.");
	}

	/**
	 * Vrac� vstupn� xml dokument.
	 * @throws JRException 
	 */
	private Document parseDocument(String inputXmlFile) throws JRException{
		Document document = null;
		if(inputXmlFile != null){
			LOGGER.debug("Parsuji xml soubor.");
			document = JRXmlUtils.parse(new File(inputXmlFile));
		}
		return document;
	}

	public void setParamsXmlFile(String paramsXmlFile){		
		this.paramsXmlFile = paramsXmlFile;
		if(paramsXmlFile!=null && paramsXmlFile.length()>0){
			JRGParamParser parser = JRGXmlParamParser.getInstance();
			putParameters(parser.parseFromFile(paramsXmlFile));
		}
	}
	
	
	
	@Override
	public Map<Object, Object> getParameters() {
		return parameters;
	}
	
	public void setDocument(Document document) {
		this.document = document;
	}

	public String getInputXmlFile(){
		return inputXmlFile;
	}

	public String getJasperFile(){
		return jasperFile;
	}

	public String getParamsXmlFile() {
		return paramsXmlFile;
	}
	
	/**
	 * Vrac� objekt reprezentuj�c� zkompilovan� report.
	 * @throws JRException 
	 */
	@Override
	public JasperReport getJasperReport() throws JRException{		
		return (JasperReport)JRLoader.loadObject(jasperFile);
	}

	@Override
	public String getOutputFile() {	
		return outputFile;
	}
	
	public void setInputXmlFile(String inputXmlFile){		
		this.inputXmlFile = inputXmlFile;
		
		try {
			document = parseDocument(inputXmlFile);
		} catch (JRException e) {
			LOGGER.error("Chyba p�i parsov�n� xml souboru s daty.", e);
		}
		
		if(document!=null){
			putParameter(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
			// parsovani parametru z elementu params
			JRGParamParser parser = JRGXmlParamParser.getInstance();
			putParameters(parser.parseFromDocument(document));
		}
	}
	
	public void setJasperFile(String jasperFile){
		this.jasperFile = JRGUtils.replaceExtension(jasperFile,"jasper");
	}
	
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}
	
	@Override
	public boolean isCompressionDemanded() {
		return compressionDemanded;
	}
	
	public void setCompressionDemanded(boolean compressionDemanded) {
		this.compressionDemanded = compressionDemanded;
	}

	@Override
	public String getFormat() {
		return format;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
}