package cz.vera.jrg.generator;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import cz.vera.jrg.app.JRGValidable;

/**
 * @author STJ
 * @version 1.1
 * @created 18-II-2010 10:05:46
 */
public interface JRGInput extends JRGValidable {
	
	/**
	 * Nastavuje vstupn� parametr �ablony
	 * 
	 * @param name
	 * @param value
	 */
	public void putParameter(String name, Object value);
	
	/**
	 * Vrac� mapu vstupn�ch parametr�
	 * 
	 * @return parametry
	 */
	public Map<Object, Object> getParameters(); 
	
	/**
	 * Vrac� data source
	 * 
	 * @return data source
	 */
	public JRDataSource getDataSource();
	
	/**
	 * Vrac� objekt reprezentuj�c� zkompilovan� report.
	 * 
	 * @throws JRException
	 * @return report 
	 */
	public JasperReport getJasperReport() throws JRException;

	/**
	 * Vrac� cestu k v�stupn�mu souboru.
	 * 
	 * @return outputFilepath
	 */
	public String getOutputFile();

	/**
	 * Vrac� p��znak, zda po exportu komprimovat v�stupn� soubor.
	 * 
	 * @return boolean
	 */
	public boolean isCompressionDemanded();
	
	/**
	 * Vrac� po�adovan� form�t v�stupu
	 * 
	 * @return format
	 */
	public String getFormat();
}