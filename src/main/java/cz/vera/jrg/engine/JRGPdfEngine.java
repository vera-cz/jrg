package cz.vera.jrg.engine;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.FontKey;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.PdfFont;

/**
 * Stroj na export PDF dokumentu.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:45
 */
@SuppressWarnings("deprecation")
public class JRGPdfEngine extends JRGAbstractEngine {

	public JRGPdfEngine(){
		exporter = new JRPdfExporter();
	}

	/**
	 * Exportuje report do souboru zadan�ho cestou.
	 * 
	 * @param destFilename
	 * @throws JRException 
	 */
	public void exportReport(String destFilename) throws JRException{			
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, destFilename);
		exporter.setParameter(JRExporterParameter.FONT_MAP, getFontMap());
		exporter.exportReport();	
	}

	
	private Map<FontKey, PdfFont> getFontMap(){
		 Map<FontKey,PdfFont> fontMap = new HashMap<FontKey,PdfFont>();
	      // Arial is Helvetica
	      fontMap.put(new FontKey("Arial", true, false), new PdfFont("Helvetica-Bold", "Cp1250", false));
	      fontMap.put(new FontKey("Arial", false, true), new PdfFont("Helvetica-Oblique", "Cp1250", false));
	      fontMap.put(new FontKey("Arial", true, true), new PdfFont("Helvetica-BoldOblique", "Cp1250", false));
	      // Times bleibt Times
	      fontMap.put(new FontKey("Times New Roman", true, false), new PdfFont("Times-Bold", "Cp1250", false));
	      fontMap.put(new FontKey("Times New Roman", false, true), new PdfFont("Times-Italic", "Cp1250", false));	      
	      fontMap.put(new FontKey("Times New Roman", true, true), new PdfFont("Times-BoldItalic", "Cp1250", false));
	      
	      return fontMap;
	}
}