package cz.vera.jrg.engine;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

/**
 * Stroj na export XLS dokumentu.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:45
 */
public class JRGXlsEngine extends JRGAbstractEngine {

	public JRGXlsEngine(){
		//pou�it JExcelApiExporter kv�li mo�nosti exportu obr�zk� 
		exporter = new JExcelApiExporter();
	}

	/**
	 * Exportuje report do souboru zadan�ho cestou.
	 * 
	 * @param destFilename
	 * @throws JRException 
	 */
	public void exportReport(String destFilename) throws JRException{
		exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, destFilename);			
		exporter.exportReport();				
	}

}