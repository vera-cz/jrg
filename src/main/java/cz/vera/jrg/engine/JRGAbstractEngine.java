package cz.vera.jrg.engine;

import java.util.HashMap;
import java.util.Map;

import cz.vera.jrg.utils.JRGUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;


/**
 * Abstraktn� stroj na export dokumentu.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:44
 */
public abstract class JRGAbstractEngine implements JRGEngine {
	protected JRDataSource dataSource;
	protected JasperPrint jasperPrint;
	protected Map<Object, Object> parameters;
	protected JRExporter exporter;
	
	
	/**
	 * Konstruktor 
	 */
	protected JRGAbstractEngine(){
		parameters = new HashMap<Object, Object>();
	}
	
	@Override
	public void setDataSource(JRDataSource dataSource) {	
		this.dataSource = dataSource;
	}
	
	/**
	 * Vytv��� instanci JasperPrint a ukl�d� ji do atributu jasperPrint.
	 * 
	 * @param jasperReport
	 * @throws JRException 
	 */
	@Override
	public void fillReport(JasperReport jasperReport) throws JRException{
		jasperReport.setWhenNoDataType(JasperReport.WHEN_NO_DATA_TYPE_NO_PAGES);
		jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);
						
		//jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		//JRDataSource ds = null;
		//jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
	}
	
	/**
	 * Vrac� po�et str�nek reportu
	 * 
	 * @return po�et str�nek reportu
	 */
	@Override
	public int getPageCount() {
		if(jasperPrint!=null && jasperPrint.getPages()!=null){
			return jasperPrint.getPages().size();
		}		
		return 0;
	}
	
	/**
	 * Vlo�� parametr pro pln�n� reportu.
	 * 
	 * @param name
	 * @param param
	 */
	@Override
	public void putParameter(Object name, Object param){
		parameters.put(name, param);
	}
	
	/**
	 * Komprimuje vygenerovan� report
	 * 
	 * @param inputReportFile
	 * @param compressedFile
	 */
	@Override
	public void compressReport(String inputReportFile) {	
		JRGUtils.zipFile(inputReportFile);
	}
}