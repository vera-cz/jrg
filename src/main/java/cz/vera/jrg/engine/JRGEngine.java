package cz.vera.jrg.engine;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

/**
 * Rozhran� stroje na export dokumentu.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:44
 */
public interface JRGEngine {
	
	/**
	 * Nastavuje data source
	 * 
	 * @param dataSource
	 */
	public void setDataSource(JRDataSource dataSource);
	
	/**
	 * Vlo�� parametr pro pln�n� reportu.
	 * 
	 * @param name
	 * @param param
	 */
	public void putParameter(Object name, Object param);

	/**
	 * Napln� report parametry.
	 * 
	 * @param jasperReport
	 */
	public void fillReport(JasperReport jasperReport) throws JRException;
	
	/**
	 * Vrac� po�et str�nek reportu
	 * 
	 * @return po�et str�nek reportu
	 */
	public int getPageCount();

	/**
	 * Exportuje report do souboru zadan�ho cestou.
	 * 
	 * @param destFilename
	 */
	public void exportReport(String destFilename) throws JRException;

	/**
	 * Komprimuje vytvo�en� report do zip souboru. 
	 * 
	 * @param inputReportFile
	 */
	public void compressReport(String inputReportFile);
}