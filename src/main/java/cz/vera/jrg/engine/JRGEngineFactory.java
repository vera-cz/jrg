package cz.vera.jrg.engine;


/**
 * Tov�rna na stroje, kter� vytv��ej� dokumenty.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:44
 */
public abstract class JRGEngineFactory {

	/**
	 * V��tov� typ typ� dokument�.
	 * 
	 * @author STJ
	 * @version 1.0
	 * @created 16-II-2010 12:00:44
	 */
	public enum EngineType {
		DOCX,
		PDF,
		RTF,
		XLS,
		XLSX,
		JPG,
		PNG;
		
		public static EngineType getInstance(String type){
			for (EngineType eType : EngineType.values()) {
				if(type.toUpperCase().equals(eType.name())){
					return eType;
				}
			}
			//pokud nebyl nalezen odpovidajici typ
			throw new IllegalArgumentException("Unknown format: "+type);
		}
	}	

	/**
	 * Priv�tn� konstruktor.
	 */
	private JRGEngineFactory(){}

	/**
	 * Vrac� stroj, podle p�ijat�ho typu (docx|pdf|rtf|xls|JPG|PNG).
	 * 
	 * @param type
	 */
	public static JRGEngine getEngine(String type){
		return getEngine(EngineType.getInstance(type));		
	}

	/**
	 * Vrac� stroj, podle p�ijat�ho v��tov�ho typu.
	 * 
	 * @param type
	 */
	public static JRGEngine getEngine(EngineType type){
		JRGEngine engine = null;
		
		switch (type){
			case DOCX:
				engine = new JRGDocxEngine();
				break;
			case PDF:
				engine = new JRGPdfEngine();
				break;
			case RTF:
				engine = new JRGRtfEngine();
				break;
			case XLS:
				engine = new JRGXlsEngine();
				break;
			case XLSX:
				engine = new JRGXlsxEngine();
				break;
			case JPG:
				engine = new JRGImageEngine(type.name().toLowerCase());
				break;
			case PNG:
				engine = new JRGImageEngine(type.name().toLowerCase());
				break;
			default:
				throw new IllegalArgumentException("Impossible format:" + type);				
		}
		
		return engine;
	}

}