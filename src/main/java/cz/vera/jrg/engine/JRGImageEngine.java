package cz.vera.jrg.engine;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter;

/**
 * Stroj na export dokumentu do obr�zku.
 * Exportuje pouze 1. str�nku reportu.
 * 
 * @author STJ
 * @version 1.0
 * @created 21-VII-2010 13:00:00
 */
public class JRGImageEngine extends JRGAbstractEngine {
	private static Logger LOGGER = Logger.getLogger(JRGImageEngine.class);
	private String format;
	
	public JRGImageEngine(String format) {
		this.format = format;
		
		try {
			exporter = new JRGraphics2DExporter();
		} catch (JRException e) {
			LOGGER.error("Nepoda�ilo se vytvo�it instanci stroje pro export do form�tu "+format, e);
		}
	}
		
	@Override
	public void exportReport(String destFilename) throws JRException {
		int pageIndex = 0; // EXPORTUJE POUZE 1. STRANKU REPORTU		

		BufferedImage pageImage = new BufferedImage(jasperPrint.getPageWidth() + 1, jasperPrint.getPageHeight() + 1, BufferedImage.TYPE_INT_RGB);
		
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRGraphics2DExporterParameter.GRAPHICS_2D, pageImage.getGraphics());
		exporter.setParameter(JRExporterParameter.PAGE_INDEX, new Integer(pageIndex));
		exporter.exportReport();
		
		saveImageToFile(destFilename, pageImage);
	}
	
	private void saveImageToFile(String destFilename, BufferedImage pageImage){
		Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix(format);
		ImageWriter writer = (ImageWriter) writers.next();
		ImageOutputStream ios;
		try {
			ios = ImageIO.createImageOutputStream(new File(destFilename));
			writer.setOutput(ios);
			writer.write(pageImage);
		} catch (IOException e) {
			LOGGER.error("Nepoda�ilo se ulo�it obr�zek do souboru "+destFilename, e);
		}
	}
}
