package cz.vera.jrg.engine;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

/**
 * Stroj na export XLSX dokumentu.
 * 
 * @author STJ
 * @version 1.0
 * @created 31-I-2012 1:00:44
 */
public class JRGXlsxEngine extends JRGAbstractEngine {
	
	public JRGXlsxEngine() {
		exporter = new JRXlsxExporter();
	}
	
	@Override
	public void exportReport(String destFilename) throws JRException {
		exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, destFilename);			
		exporter.exportReport();	
	}

}
