package cz.vera.jrg.engine;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;

/**
 * Stroj na export RTFdokumentu.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:45
 */
public class JRGRtfEngine extends JRGAbstractEngine {

	public JRGRtfEngine(){
		exporter = new JRRtfExporter();
	}

	/**
	 * Exportuje report do souboru zadan�ho cestou.
	 * 
	 * @param destFilename
	 * @throws JRException 
	 */
	public void exportReport(String destFilename) throws JRException{
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, destFilename);			
		exporter.exportReport();	
	}

}