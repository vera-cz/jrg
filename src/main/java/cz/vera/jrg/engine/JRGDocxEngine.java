package cz.vera.jrg.engine;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;

/**
 * Stroj na export DOCX dokumentu.
 * 
 * @author STJ
 * @version 1.0
 * @created 16-II-2010 12:00:44
 */
public class JRGDocxEngine extends JRGAbstractEngine {

	public JRGDocxEngine(){
		exporter = new JRDocxExporter();
	}

	/**
	 * Exportuje report do souboru zadan�ho cestou.
	 * 
	 * @param destFilename
	 * @throws JRException 
	 */
	public void exportReport(String destFilename) throws JRException{
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, destFilename);			
		exporter.exportReport();	
	}

}