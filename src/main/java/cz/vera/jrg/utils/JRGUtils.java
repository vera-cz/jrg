package cz.vera.jrg.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

public class JRGUtils {
	public static final Logger LOGGER = Logger.getLogger(JRGUtils.class);
	public static final String SEPARATOR = System.getProperty("file.separator");
		

	/**
	 * Vrac� p��ponu souboru.
	 * Pokud je soubor null, nebo nem� p��ponu, vrac� pr�zdn� �et�zec.
	 * 
	 * @param fileName
	 * @return extension
	 */
	public static String getExtension(String fn){
		String ext = "";
		if(fn != null){	
			if(fn.lastIndexOf(".") == -1){
				ext = "";
			}else{
				ext = fn.substring(fn.lastIndexOf(".")+1).toLowerCase();
			}
		}
		return ext;
	}

	/**
	 * 
	 * @param fn
	 * @param ext
	 * @return
	 */
	public static String replaceExtension(String fn, String ext){
		String fnout = fn;
		if(ext==null || ext.length()==0){
			return fnout; //========>
		}
		if(fn!=null && fn.length()>0){
			int index = fn.lastIndexOf("."+getExtension(fn));
			if(index>1){
				fnout = fn.substring(0, index) + "." + ext;
			}else{
				fnout = fn + "." + ext;
			}
		}
		return fnout;
	}
	
	/**
	 * Vrac� n�zev souboru ze zadan� cesty k souboru.
	 * 
	 * @param filePath
	 * @return fileName
	 */
	public static String getFileNameFromPath(String filePath){
		String fn = null;
		if(filePath!=null){
			int pos = filePath.lastIndexOf(SEPARATOR);
			if(pos > 0){
				if(pos == filePath.length()-1){
					fn = null;
				}else{
					fn = filePath.substring(pos+1).trim();
				}				
			}else{
				fn = filePath.trim();
			}
		}		
		return fn;
	}

	/**
	 * Zipuje soubor.
	 * 
	 * @param inputFile
	 * @param compressedFile
	 */
	public static void zipFile(String source){		  
	    // Create a buffer for reading the files
	    byte[] buf = new byte[1024];
	    
	    try {
	        // Create the ZIP file
	        String target = replaceExtension(source, "zip");
	        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(target));
	    
            FileInputStream in = new FileInputStream(source);
    
            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(getFileNameFromPath(source)));
    
            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
    
            // Complete the entry
            out.closeEntry();
            in.close();
	    
	        // Complete the ZIP file
	        out.close();
		}catch(IOException ioe){
			LOGGER.error("Nepoda�ilo se zkomprimovat soubor:" + source, ioe);
		}
	
	}

}
