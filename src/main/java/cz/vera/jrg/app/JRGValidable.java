package cz.vera.jrg.app;

/**
 * Rozhran� pro validaci atribut� objektu.
 * 
 * @author stj
 *
 */
public interface JRGValidable {
	
	/**
	 * Prov�d� validaci.
	 * 
	 * @throws IllegalArgumentException
	 */
	public void validate() throws IllegalArgumentException;
}
