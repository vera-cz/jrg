package cz.vera.jrg.app;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;

/**
 * Konfigur�tor logov�n� JRG
 * 
 * @author stj
 *
 */
public class JRGLogConfigurator {

	static String LOG4J_PROPERTIES_FILE="log4j.properties";
	static String LOG_FILE="jrg.log";
	static String JASPERDIR_ENV = "JASPERDIR";
	static String RADNICEDIR_ENV = "RADNICEDIR";
	static String RADLOG_ENV = "RADLOG";
	static String JASPERLOG_DIR_VARIABLE_NAME = "java.jasperlog.dir";

	/**
	 * Nakonfiguruje Log4j podle nastaven� prom�nn�ch prost�ed�.  
	 */
	static void configure(){
		Map<String, String> env = System.getenv();
		
		String radDir 	  = env.get(JRGLogConfigurator.RADNICEDIR_ENV);
		String jrgDir 	  = env.get(JRGLogConfigurator.JASPERDIR_ENV);
		String radlogDir  = env.get(JRGLogConfigurator.RADLOG_ENV);
		
		JRGLogConfigurator.setJasperlogDirVariable(radlogDir, radDir);
		String path = JRGLogConfigurator.getLogPropertiesFilePath(radDir, jrgDir);

		File log = new File(path);
		if(log.exists()){
			PropertyConfigurator.configure(path);	
		}else{
			if(jrgDir!=null && jrgDir.length()>0){			
				// vytvari jrg.log v JASPERDIR
				String logfile = jrgDir+File.separator+JRGLogConfigurator.LOG_FILE;
				PatternLayout pl = new PatternLayout(PatternLayout.TTCC_CONVERSION_PATTERN);
				try {
					FileAppender fa  = new FileAppender(pl, logfile);
					BasicConfigurator.configure(fa);
				} catch (IOException e) {
					e.printStackTrace();
					BasicConfigurator.configure();
				}
			}else{
				// defaultni vystup do konzole
				BasicConfigurator.configure();
			}			
			Logger.getRootLogger().setLevel(Level.ERROR);
		}	
	}

	/**
	 * Nastavuje javovskou prom�nnou java.jasperlog.dir pro nastaven� cesty
	 * logovac�ho souboru do $RADLOG, p��padn� $RADNICEDIR/logs v log4j.properties.
	 *  
	 * @param radlogDir
	 * @param radDir
	 */
	private static void setJasperlogDirVariable(String radlogDir, String radDir){
		if(radlogDir!=null && radlogDir.length()>0){
			System.setProperty(JRGLogConfigurator.JASPERLOG_DIR_VARIABLE_NAME, radlogDir);
		}else{
			String dir = radDir+File.separator+"logs";
			System.setProperty(JRGLogConfigurator.JASPERLOG_DIR_VARIABLE_NAME, dir);
		}
	}

	/**
	 * Vrac� cestu, kde hledat konfigura�n� soubor log4j.properties, 
	 * podle nastaven� prom�nn�ch prost�ed�.
	 * 
	 * @param radDir
	 * @param jrgDir
	 * @return cesta k log4j.properties
	 */
	private static String getLogPropertiesFilePath(String radDir, String jrgDir){
		String path = null;
		
		if(radDir!=null && radDir.length()>0){
			path = radDir+File.separator+"etc"+File.separator+JRGLogConfigurator.LOG4J_PROPERTIES_FILE;
		}else if(jrgDir!=null && jrgDir.length()>0){
			path = jrgDir+File.separator+JRGLogConfigurator.LOG4J_PROPERTIES_FILE;
		}else{
			path = JRGLogConfigurator.LOG4J_PROPERTIES_FILE;
		}
		
		return path;
	}

}
