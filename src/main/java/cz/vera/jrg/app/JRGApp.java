package cz.vera.jrg.app;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import cz.vera.jrg.engine.JRGEngine;
import cz.vera.jrg.engine.JRGEngineFactory;
import cz.vera.jrg.generator.JRGGenerator;
import cz.vera.jrg.generator.JRGInput;
import cz.vera.jrg.generator.JRGXmlInput;

/**
 * Spou�tec� t��da aplikace.
 * 
 * @author STJ
 * @version 1.5
 * @created 16-II-2010 12:00:44
 */
@SuppressWarnings("static-access")
public class JRGApp {
	public static final String VERSION = "2.0.4";
	private static Logger LOGGER = Logger.getLogger(JRGApp.class);
	
	//argumenty z prikazove radky
	private static final Options options;
	
	static{
		Option format = OptionBuilder.hasArg().withArgName(JRGArg.FORMAT.getName())
						.withDescription("* form�t generovan�ho v�stupu pdf|xls|rtf|docx|png|jpg")
						.create(JRGArg.FORMAT.getOption());
		
		Option zip 	  = OptionBuilder
						.withDescription("zip komprimace v�stupu\n")
						.create(JRGArg.ZIP.getOption());
		
		Option data	  = OptionBuilder.hasArg().withArgName(JRGArg.DATA.getName())
						.withDescription("* cesta k datov�mu xml souboru")
						.create(JRGArg.DATA.getOption());
						
		Option params = OptionBuilder.hasArg().withArgName(JRGArg.PARAMS.getName())
						.withDescription("cesta ke xml souboru s parametry �ablony")
						.create(JRGArg.PARAMS.getOption());
		
		Option templ  = OptionBuilder.hasArg().withArgName(JRGArg.TEMPLATE.getName())
						.withDescription("* cesta k jasper �ablon�")
						.create(JRGArg.TEMPLATE.getOption());
		
		Option output = OptionBuilder.hasArg().withArgName(JRGArg.OUTPUT.getName())
						.withDescription("* cesta k v�stupn�mu souboru")
						.create(JRGArg.OUTPUT.getOption());
		
		options = new Options();
		options.addOption(format);
		options.addOption(zip);
		options.addOption(data);
		options.addOption(params);
		options.addOption(templ);
		options.addOption(output);
	}
	
	/**
	 * Konstruktor
	 */
	public JRGApp(){
	}

	/**
	 * Spou�t�c� metoda aplikace.
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		JRGLogConfigurator.configure();
		new JRGApp().start(args);
	}

	private void start(String[] args){
		LOGGER.info(getStartMessage(args));
		
		if(args.length == 4 || args.length == 5){
			// P�vodn� zp�sob zpracov�n� p��k. ��dky, do verze 0.8.2
			start_0_8_2(args);
		}else{
			// nov�, pomoc� options
			generate(parseArgs(args));
		}		
	}
	

	private void generate(JRGInput input){
		long start = System.currentTimeMillis();
		
		JRGEngine engine = JRGEngineFactory.getEngine(input.getFormat());
		JRGGenerator gen = new JRGGenerator(engine);
		LOGGER.debug("Spoustim export.");
		gen.generate(input);
		
		long runningTime = (System.currentTimeMillis() - start);
		String rtStr = "running time : " + runningTime + " ms";				
		LOGGER.info(rtStr);				
	}

	
	private JRGInput parseArgs(String[] args ){
		CommandLineParser parser = new GnuParser();
		
		JRGXmlInput input = new JRGXmlInput();
		
		try {
			CommandLine line = parser.parse(options, args);
			if(	   !line.hasOption(JRGArg.FORMAT.getOption())
				|| !line.hasOption(JRGArg.DATA.getOption())
				|| !line.hasOption(JRGArg.TEMPLATE.getOption())
				|| !line.hasOption(JRGArg.OUTPUT.getOption()))
			{
				throw new ParseException("Nebyl zad�n n�kter� z povinn�ch parametr�.");
			}else{				
				input.setFormat(line.getOptionValue(JRGArg.FORMAT.getOption()));
				input.setInputXmlFile(line.getOptionValue(JRGArg.DATA.getOption()));
				input.setJasperFile(line.getOptionValue(JRGArg.TEMPLATE.getOption()));
				input.setOutputFile(line.getOptionValue(JRGArg.OUTPUT.getOption()));
				
				if(line.hasOption(JRGArg.PARAMS.getOption())){
					input.setParamsXmlFile(line.getOptionValue(JRGArg.PARAMS.getOption()));
				}
				
				if(line.hasOption(JRGArg.ZIP.getOption())){
					input.setCompressionDemanded(true);
				}
								
				input.validate();
			}			
			
		} catch (ParseException e) {
			LOGGER.error("Nepoda�ilo se zpracovat argumenty p��kazov� ��dky.", e);
			HelpFormatter hf = new HelpFormatter();
			hf.printHelp("cz.vera.jrg.JRGApp <args..>", options);
			System.exit(0);
		}
		
		return input;
	}

	private String getStartMessage(String[] args){
		StringBuffer buf = new StringBuffer();
		buf.append("JRG "+VERSION+" spusteno s parametry:");
		for (String arg : args) {
			buf.append(arg);
			buf.append(" ");
		}
		return buf.toString();
	}

	
// ========================================================================================	
// == P�vodn� zp�sob zpracov�n� p��k. ��dky, do verze 0.8.2 ===============================

	private void start_0_8_2(String[] args){
		try{
			if(args.length == 4 || args.length == 5){				
				long start = System.currentTimeMillis();
				
				JRGXmlInput input = parseInputArguments(args);								
				JRGEngine engine = JRGEngineFactory.getEngine(args[0].trim());
				JRGGenerator gen = new JRGGenerator(engine);
				LOGGER.debug("Spoustim export.");
				gen.generate(input);
				
				long runningTime = (System.currentTimeMillis() - start);
				String rtStr = "running time : " + runningTime + " ms";				
				LOGGER.info(rtStr);				
			}else{
				String errmsg = "Zad�n nespr�vn� po�et parametr�:"+args.length+"\n"+getHelpMessage(); 
				throw new IllegalArgumentException(errmsg);
			}
		}catch(Exception e){
			if(!LOGGER.isInfoEnabled()){			
				LOGGER.error(getStartMessage(args));
			}
			LOGGER.error("", e);
			System.exit(0);
		}				
	}

	private JRGXmlInput parseInputArguments(String args[]){
		JRGXmlInput input = new JRGXmlInput();
		int posun = 0;
		if(args.length == 5){
			String zip = args[1];
			if(zip.trim().equals("-z")){
				input.setCompressionDemanded(true);
				posun++;
			}else{
				throw new IllegalArgumentException("Nespr�vn� parametr: "+args[1]+"   O�ek�van� parametr: -z");
			}
		}		
		
		input.setInputXmlFile(args[posun+1].trim());
		input.setJasperFile(args[posun+2].trim());
		input.setOutputFile(args[posun+3].trim());
		
		input.validate();
		
		return input;
	}
					
	private static String getHelpMessage(){
		StringBuilder sb = new StringBuilder();	
		sb.append("Prijima parametry:\n");
		sb.append("\t\t");
		sb.append("typ generovan�ho vystupu pdf|xls|rtf|docx\n");
		sb.append("\t\t");
		sb.append("volitelny parametr -z (zda zazipovat vystup. soubor)\n");
		sb.append("\t\t");
		sb.append("cesta k vstupnimu xml souboru\n");
		sb.append("\t\t");
		sb.append("jasper sablona\n");				
		sb.append("\t\t");
		sb.append("vystupni soubor\n");				
		return sb.toString();
	}
// ========================================================================================
	
}