package cz.vera.jrg.app;

public enum JRGArg {
	FORMAT	("f", "format"),
	ZIP		("z", "zip"),
	DATA	("d", "data"),
	PARAMS	("p", "params"),
	TEMPLATE("t", "template"),
	OUTPUT	("o", "output")
	;
	
	private String option;
	private String name;

	private JRGArg(String option, String name){
		this.option = option;
		this.name   = name;
	}
	
	public String getOption() {
		return option;
	}
	
	public String getName() {
		return name;
	}
}
