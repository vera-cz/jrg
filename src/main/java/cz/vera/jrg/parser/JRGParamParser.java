package cz.vera.jrg.parser;

import java.util.Map;

import org.w3c.dom.Document;

/**
 * Rozhraní pro parsování parametrů
 * 
 * @author stj
 *
 */
public interface JRGParamParser {
	
	/**
	 * Parsuje parametry ze souboru
	 * 
	 * @param  fileName
	 * @return parametry <nazev, hodnota> 
	 */
	public Map<String, Object> parseFromFile(String filePath);
	
	/**
	 * Parsuje parametry z Documentu
	 *  
	 * @param doc
	 * @return parametry <nazev, hodnota>
	 */
	public Map<String, Object> parseFromDocument(Document doc);
}
