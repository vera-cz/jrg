package cz.vera.jrg.parser;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.util.JRXmlUtils;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.org.apache.bcel.internal.generic.NEW;

/**
 * Parsuje parametry z xml
 * 
 * @author stj
 *
 */
public class JRGXmlParamParser implements JRGParamParser {
	private static final Logger LOGGER = Logger.getLogger(JRGXmlParamParser.class);
	private static final String PARAMETER_CLASS = "type";
	
	private JRGXmlParamParser(){}
	
	public static JRGParamParser getInstance(){
		return new JRGXmlParamParser();
	}
	
	@Override
	public Map<String, Object> parseFromFile(String filePath) {
		Map<String, Object> params = null;

		LOGGER.debug("Parsuji xml soubor s parametry.");
		try {
			Document doc = JRXmlUtils.parse(new File(filePath));
			params = parseFromDocument(doc);
		} catch (JRException e) {
			LOGGER.error("Chyba p�i parsov�n� xml s parametry.", e);
		}
		
		return params;
	}
	
	@Override
	public Map<String, Object> parseFromDocument(Document doc){
		LOGGER.debug("Parsuji parametry z Documentu.");
		Map<String, Object> params = new HashMap<String, Object>();
		
		if(doc!=null){
		    Element root = doc.getDocumentElement();
            root.normalize();
            NodeList nlParams = root.getElementsByTagName("params");
            if(nlParams!=null && nlParams.getLength()>0){
	            Node elParams = nlParams.item(0);
	            
	            // proch�z� parametry, z�sk�v� jejich hodnoty a ukl�d� je do mapy
            	NodeList nodeList = elParams.getChildNodes();
	            for (int i = 0; i < nodeList.getLength(); i++) {
					Node node = nodeList.item(i);
					if(node.getNodeType()==Node.ELEMENT_NODE){
						// z�sk�n� hodnoty parametru
						Element element = (Element)node;
						String name  = element.getNodeName();
						String value = element.getTextContent();
						if (value!=null && value.equals("")){
							value = null;
						}
						
						//p�etypov�n� pokud obsahuje atribut type
						Object valueObj = null;
						String paramType = element.getAttribute(PARAMETER_CLASS);
						valueObj = castParam(value, paramType);
						
						//ulo�en� do mapy
						params.put(name, valueObj);
						LOGGER.trace("Param "+name+": "+value);
					}				
				}
            }
		}
		
		return params;
	}
	
	/**
	 * P�etypuje hodnotu na spr�vn� typ, pokud je ur�en atribut type, jinak z�st�v� jako String
	 * @param value
	 * @param paramType
	 * @return valueObj
	 */
	private Object castParam(String value, String paramType){
		Object valueObj = value;
		
		if (value!= null && paramType!=null && !paramType.equals("")){			
			if(		 paramType.equals("java.lang.Boolean")){
				valueObj = Boolean.valueOf(value);
			}else if(paramType.equals("java.lang.Byte")){
				valueObj = Byte.valueOf(value);
			}else if(paramType.equals("java.lang.Double")){
				valueObj = Double.valueOf(value);
			}else if(paramType.equals("java.lang.Float")){
				valueObj = Float.valueOf(value);
			}else if(paramType.equals("java.lang.Integer")){
				valueObj = Integer.valueOf(value);
			}else if(paramType.equals("java.lang.Long")){
				valueObj = Long.valueOf(value);
			}else if(paramType.equals("java.lang.Short")){
				valueObj = Short.valueOf(value);
			}else if(paramType.equals("java.math.BigDecimal")){
				valueObj = new BigDecimal(value);
			}			
			
			// else .. jin� t��dy
		}
		
		return valueObj;
	}
}
